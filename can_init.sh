#!/bin/bash
if [ "$1" == "uninit" ]
then
  sudo /sbin/ip link set can1 down
fi

config-pin p9.24 can
config-pin p9.26 can 

sudo /sbin/ip link set can1 up type can bitrate 100000
sudo ifconfig can1 txqueuelen 4096
sudo ifconfig can1 up
