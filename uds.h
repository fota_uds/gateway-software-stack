#pragma once

#include <stdint.h>
#include <stdio.h>

int8_t reset_service(uint8_t ecu_id, char *filename);
void *bb_can_receive(void *ptr); 
char * request_data_by_id(uint8_t ecu_id, uint8_t data_id);
