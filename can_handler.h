#pragma once
#include <stdint.h>
#include <stdbool.h>

#include "isotp.h"


bool can__init();


bool can__tx(can__num_e can, can__msg_t *msg_ptr, uint32_t timeout_ms);
bool can__rx(can__msg_t *can_message_ptr);