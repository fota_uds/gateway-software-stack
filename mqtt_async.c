#include "MQTTAsync.h"
#include "uds.h"
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
//#include "download_file.h"
#include "md5.h"
#include <stdbool.h>

#define ADDRESS "tcp://18.144.135.55:1883"
#define CLIENTID "GatewayDeviceCode1"
#define TOPIC "fwData"
#define PAYLOAD "Hello World!"
#define QOS 1
#define TIMEOUT 10000L
#define PEM_KEY_PATH "~/california_ec2.pem"

typedef struct payload {
  char ecu_id;
  char filename[255];
}payload;

volatile MQTTAsync_token deliveredtoken;
int disc_finished = 0;
int subscribed = 0;
int finished = 0;
uint8_t ecu_id= 0x88;
//static char *PEM_KEY_PATH = "";
static MQTTAsync client;
static MQTTAsync_connectOptions conn_opts =
    MQTTAsync_connectOptions_initializer;
static MQTTAsync_disconnectOptions disc_opts =
    MQTTAsync_disconnectOptions_initializer;
static MQTTAsync_message pubmsg = MQTTAsync_message_initializer;
static MQTTAsync_token token;

void publishData(const char *topic, char *send_data);
void connlost(void *context, char *cause) {
  MQTTAsync client = (MQTTAsync)context;
  MQTTAsync_connectOptions conn_opts = MQTTAsync_connectOptions_initializer;
  int rc;
  printf("\nConnection lost\n");
  printf("     cause: %s\n", cause);
  printf("Reconnecting\n");
  conn_opts.keepAliveInterval = 20;
  conn_opts.cleansession = 1;
  if ((rc = MQTTAsync_connect(client, &conn_opts)) != MQTTASYNC_SUCCESS) {
    printf("Failed to start connect, return code %d\n", rc);
    finished = 1;
  }
}

bool calculate_md5(char *filename, char *md5){

    if (!CalcFileMD5(filename, md5)) {
        return false;
    } else {
	return true;
    }
}

bool download_fw(char *str, payload *fw_data){
  char *url=NULL;
  char *str_ecu_id=NULL;
  char *rcv_md5=NULL;
  url = strtok (str,",");
  str_ecu_id = strtok (NULL, ",");
  fw_data->ecu_id = atoi(str_ecu_id);
  rcv_md5 = strtok (NULL, ",");
  sprintf(fw_data->filename, "%p.bin",fw_data->ecu_id);
  printf("URL = %s\n", url);
  printf("outfilename = %s\n", fw_data->filename);
  printf("ECU_ID=%s\n", str_ecu_id);
  //downloadFile(url, outfilename);
  char scp_cmd[1000]="";
  sprintf(scp_cmd,"scp -i %s ubuntu@18.144.135.55:%s %s",PEM_KEY_PATH,url,fw_data->filename);
  printf("scp command  = %s\n", scp_cmd);
  if(system(scp_cmd)==-1) return 255;
  char md5[MD5_LEN + 1];
  calculate_md5(fw_data->filename, md5);
  printf("md5 = %s\n", md5);
  if(strcmp(rcv_md5, md5)){
  	return false;
  }
  return true;
}

void transfer_fw(char *fw_bin, size_t size) {
  FILE *fp = fopen("test.bin", "wb");
  if (!fp) {
    printf("unable to open file\n");
    exit(-1);
  }
  fwrite(fw_bin, 1, size, fp);
  fclose(fp);
  // reset_service(0x88);
}


void *thread_for_reset(void *args) {
  payload *fw_data = (payload *)args;
  printf("Reset ECU for = %x\n", (fw_data->ecu_id));
  if(reset_service(fw_data->ecu_id, fw_data->filename)<0){
  	printf("reset service failed for ecu id = %d\n", fw_data->ecu_id);
  }
  free(fw_data);
  return NULL;
}

void* get_debug_data(void *args){
  char *str_ecu_id=NULL;
  char *debug_rq = (char *)args;
  char *str_did=NULL;
  str_ecu_id = strtok (debug_rq, ",");
  uint8_t ecu_id = atoi(str_ecu_id);
  str_did = strtok (NULL, ",");
  uint8_t did = atoi(str_did);
  printf("ECU_ID = %x, DID = %d\n", ecu_id, did);
  char *data = request_data_by_id(ecu_id, did);
  printf("Data = %s\n", data);
  publishData("debug_rcv", data);
  if(data) free(data);
  if(args) free(args);
}

int msgarrvd(void *context, char *topicName, int topicLen,
             MQTTAsync_message *message) {
  int i;
  //   char *payloadptr;
  printf("Message arrived\n");
  printf("     topic: %s\n", topicName);
  //   printf("   message: ");
  //   payloadptr = message->payload;i
  payload *fw_data = (payload *)malloc(1*sizeof(payload));
  if(!strcmp(topicName, TOPIC)){
  	//transfer_fw(message->payload, message->payloadlen);
  	bool result=download_fw((char *)message->payload, fw_data);
  	if(result){
	  pthread_t thread_id;
  	  printf("ECU ID = %x\n", fw_data->ecu_id);
  	  printf("filename = %s\n", fw_data->filename);
  	  pthread_create(&thread_id, NULL, thread_for_reset, (void *)(fw_data));
	}else{
	  printf("Invalid request\n");
	}
  	//publishData("DEBUG_SPEED", "10");
  }
  else if(!strcmp(topicName, "debug")){
	size_t req_len = strlen(message->payload);
  	char *debug_rqst = (char *)malloc(req_len + 1);
	strcpy(debug_rqst, message->payload);
	pthread_t tid;
	pthread_create(&tid, NULL, get_debug_data, (void *)debug_rqst);
  }
	MQTTAsync_freeMessage(&message);
  	MQTTAsync_free(topicName);
  return 1;
}



void onDisconnect(void *context, MQTTAsync_successData *response) {
  printf("Successful disconnection\n");
  disc_finished = 1;
}
void onSubscribe(void *context, MQTTAsync_successData *response) {
  printf("Subscribe succeeded\n");
  subscribed = 1;
}
void onSubscribeFailure(void *context, MQTTAsync_failureData *response) {
  printf("Subscribe failed, rc %d\n", response ? response->code : 0);
  finished = 1;
}
void onConnectFailure(void *context, MQTTAsync_failureData *response) {
  printf("Connect failed, rc %d\n", response ? response->code : 0);
  finished = 1;
}

void publishData(const char *topic, char *send_data) {
  MQTTAsync_responseOptions opts = MQTTAsync_responseOptions_initializer;
  MQTTAsync_message pubmsg = MQTTAsync_message_initializer;
  int rc;
  pubmsg.payload = send_data;
  pubmsg.payloadlen = strlen(send_data);
  if ((rc = MQTTAsync_sendMessage(client, topic, &pubmsg, &opts)) !=
      MQTTASYNC_SUCCESS) {
    printf("Failed to start sendMessage, return code %d\n", rc);
    exit(EXIT_FAILURE);
  }
}

void onConnect(void *context, MQTTAsync_successData *response) {
  MQTTAsync client = (MQTTAsync)context;
  MQTTAsync_responseOptions opts = MQTTAsync_responseOptions_initializer;
  MQTTAsync_message pubmsg = MQTTAsync_message_initializer;
  int rc;
  printf("Successful connection\n");
  printf("Subscribing to topic %s\nfor client %s using QoS%d\n\n"
         "Press Q<Enter> to quit\n\n",
         TOPIC, CLIENTID, QOS);
  opts.onSuccess = onSubscribe;
  opts.onFailure = onSubscribeFailure;
  opts.context = client;
  deliveredtoken = 0;
  if ((rc = MQTTAsync_subscribe(client, TOPIC, QOS, &opts)) !=
      MQTTASYNC_SUCCESS) {
    printf("Failed to start subscribe, return code %d\n", rc);
    exit(EXIT_FAILURE);
  }
  if ((rc = MQTTAsync_subscribe(client, "debug", QOS, &opts)) !=
      MQTTASYNC_SUCCESS) {
    printf("Failed to start subscribe, return code %d\n", rc);
    exit(EXIT_FAILURE);
  }

}


int main(int argc, char *argv[]) {
  //   MQTTAsync client;
  //   MQTTAsync_connectOptions conn_opts =
  //   MQTTAsync_connectOptions_initializer; MQTTAsync_disconnectOptions
  //   disc_opts =
  //       MQTTAsync_disconnectOptions_initializer;
  //   MQTTAsync_message pubmsg = MQTTAsync_message_initializer;
  //   MQTTAsync_token token;
  int rc;
  int ch;
  pthread_t can_rcv_tid;
  pthread_create(&can_rcv_tid, NULL, bb_can_receive, NULL);
  MQTTAsync_create(&client, ADDRESS, CLIENTID, MQTTCLIENT_PERSISTENCE_NONE,
                   NULL);
  MQTTAsync_setCallbacks(client, NULL, connlost, msgarrvd, NULL);
  conn_opts.keepAliveInterval = 20;
  conn_opts.cleansession = 1;
  conn_opts.onSuccess = onConnect;
  conn_opts.onFailure = onConnectFailure;
  conn_opts.context = client;
  if ((rc = MQTTAsync_connect(client, &conn_opts)) != MQTTASYNC_SUCCESS) {
    printf("Failed to start connect, return code %d\n", rc);
    exit(EXIT_FAILURE);
  }
  while (!subscribed)
#if defined(WIN32) || defined(WIN64)
    Sleep(100);
#else
    usleep(10000L);
#endif
  if (finished)
    goto exit;
  do {
    ch = getchar();
  } while (ch != 'Q' && ch != 'q');
  disc_opts.onSuccess = onDisconnect;
  if ((rc = MQTTAsync_disconnect(client, &disc_opts)) != MQTTASYNC_SUCCESS) {
    printf("Failed to start disconnect, return code %d\n", rc);
    exit(EXIT_FAILURE);
  }
  while (!disc_finished)
#if defined(WIN32) || defined(WIN64)
    Sleep(100);
#else
    usleep(10000L);
#endif
exit:
  pthread_join(can_rcv_tid, NULL);
  return rc;
}

