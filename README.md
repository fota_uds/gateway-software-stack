# FOTA
C code for SocketCAN on Linux

* Based on documentation at: https://www.kernel.org/doc/Documentation/networking/can.txt

To install VCAN0:
```
$ ip link add dev vcan0 type vcan
```
Use gcc to build examples:
```
$ gcc main.c can_handler.c isotp.c uds.c -o run -lpthread
```
* Build MQTT client library
git clone https://github.com/eclipse/paho.mqtt.c
cd paho.mqtt.c
make	#Builds the library
sudo make install	#Installs the shared library
```


gcc -I ../curl/include/  download_file.c -lcurl

Compile using mqtt shared library
gcc mqtt.c -lpaho-mqtt3cs -o mqtt
gcc -I ../curl/include/ mqtt_async.c uds.c can_handler.c isotp.c download_file.c md5.c -lpaho-mqtt3as -lpthread -lcurl -o gateway_sw
gcc mqtt_async.c uds.c can_handler.c isotp.c md5.c -lpaho-mqtt3as -lpthread -o gateway_sw
```
