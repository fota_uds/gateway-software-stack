// isotp.c
#include "isotp.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <unistd.h>
#include "can_handler.h"

static const uint8_t MAX_PDU_SIZE = 0x07;
static const uint8_t single_frame = 0x00;
static const uint8_t first_frame = 0x01;
static const uint8_t consecutive_frame = 0x02;
static const uint8_t flow_control_frame = 0x3;

// QueueHandle_t Flow_control_queue = NULL;

void delay__us(int val){
    usleep(val);
}

void calculate_speration_time_in_us(uint8_t sep_time_in_flow_control_frame, int *sep_time_in_us) {
  *sep_time_in_us = (sep_time_in_flow_control_frame & 0x0F) * 100;
}

void isotp__init(isotp_params__s *message_param, void *rcv_data_ptr, size_t rcv_data_size_in_bytes) {
  message_param->recieve_buffer_offset = 0;
  message_param->recieve_buffer = rcv_data_ptr;
  message_param->recieve_buffer_size = rcv_data_size_in_bytes;
  message_param->fc_info.fc_flag = CONTINUE;
  message_param->fc_info.sep_time = 100;
  message_param->fc_info.block_size = 0;
}

bool isotp_tx(isotp_params__s *message_param, can__num_e can, void *data_ptr, size_t data_size_in_bytes,
              bool is_control_frame) {

  // exit the function if any one of the pointers are NULL;
  if ((message_param == NULL) && (data_ptr == NULL))
    return false;

  uint8_t timeout = 0;
  can__msg_t can_message_ptr;
  bool result = false;
  uint8_t *temp_data_ptr = (uint8_t *)data_ptr;
  int tx_data_size_left = data_size_in_bytes;
  can_message_ptr.msg_id = message_param->CAN_ID;

  // flow_control_s rx_queue;

  // Single packet transmission (data size <= 7 bytes)
  if (!is_control_frame && MAX_PDU_SIZE >= data_size_in_bytes) {
    can_message_ptr.data.bytes[0] = ((uint8_t)0x0F & data_size_in_bytes);
    can_message_ptr.data.bytes[0] |= ((uint8_t)0xF0 & (single_frame << 4)); // byte 0, upper nibble
    can_message_ptr.frame_fields.data_len = data_size_in_bytes + 1;
    for (int i = 0; i < data_size_in_bytes; i++) {
      can_message_ptr.data.bytes[i + 1] = temp_data_ptr[i];
    }

    if (can__tx(can, &can_message_ptr, timeout))
      result = true;
  }

  // transmit flow control frame
  else if (is_control_frame) {

    can_message_ptr.data.bytes[0] = ((uint8_t)0x0F & message_param->fc_info.fc_flag);
    // can_message_ptr.data.bytes[0] |= ((uint8_t)0xF0 & (flow_control_frame << 4)); // byte 0, upper nibble
    can_message_ptr.data.bytes[0] |= ((uint8_t)0xF0 & (flow_control_frame << 4)); // byte 0, upper nibble
    can_message_ptr.data.bytes[1] = message_param->fc_info.block_size;
    can_message_ptr.data.bytes[2] = message_param->fc_info.sep_time;
    can_message_ptr.frame_fields.data_len = 3;

    if (can__tx(can, &can_message_ptr, timeout))
      result = true;
  }

  // Multiple packet transmission
  else if (MAX_PDU_SIZE < data_size_in_bytes) {
	  //printf("data size = %d\n", data_size_in_bytes);
    can_message_ptr.data.bytes[0] = ((uint8_t)0x0F & data_size_in_bytes); // byte 0, lower nibble
    // can_message_ptr.data.bytes[0] |= ((uint8_t)0xF0 & (first_frame << 4)); // byte 0, upper nibble
    can_message_ptr.data.bytes[0] |= ((uint8_t)0xF0 & (first_frame << 4));     // byte 0, upper nibble
    can_message_ptr.data.bytes[1] = (uint8_t)(data_size_in_bytes >> 4) & 0xff; // byte 1

    for (int first_frame_counter = 0; first_frame_counter < 6; first_frame_counter++) {
      // can_message_ptr.data.bytes[i + 2] = temp_data_ptr[i];
      can_message_ptr.data.bytes[first_frame_counter + 2] = *(temp_data_ptr++);
    }
    can_message_ptr.frame_fields.data_len = 8;
    if (can__tx(can, &can_message_ptr, timeout)) // check for sucessful transmission of first frame
    {
      tx_data_size_left -= 6;
      uint8_t number_of_frames = ((data_size_in_bytes - 6) / 7); // total frames excluding first frame
      if ((data_size_in_bytes - 6) % 7)
        number_of_frames++;

      //
      uint8_t fw_check_frame = 1;
      uint8_t consecutive_frame_index = 1; // ranges from (0 to number_of_frames - 1)
      int speration_time_in_us = 100;
      bool is_fc_required = true;

      // while (xQueueReceive(Flow_control_queue, &message_param->fc_info, 100) && (byte_count > 0)) // wait for flow
      // control frame
      // change below if top while
      // wait for flow control frame
      do {
        while (is_fc_required && message_param->fc_info.fc_flag == WAIT) {
          delay__us(100);
        }
        if (is_fc_required && message_param->fc_info.fc_flag == ABORT)
          return false;
        else if (is_fc_required && message_param->fc_info.fc_flag == COMPLETE)
          return true;

        uint8_t blocks = message_param->fc_info.block_size;
        if (blocks == 0) {
          is_fc_required = false;
          speration_time_in_us = 10*1000; // if block size = 0, sep time is 127 ms
        } else {
          if (!(fw_check_frame % blocks)) { // Need to test this scenario
            calculate_speration_time_in_us(message_param->fc_info.sep_time, &speration_time_in_us);
            is_fc_required = true;
          } else {
            is_fc_required = false;
          }
        }
        can_message_ptr.data.bytes[0] = ((uint8_t)0x0F & consecutive_frame_index);
        can_message_ptr.data.bytes[0] |= ((uint8_t)0xF0 & (consecutive_frame << 4)); // byte 0, upper nibble
        size_t data_transmit_bytes_in_frame = (tx_data_size_left > 7) ? 7 : tx_data_size_left;
        can_message_ptr.frame_fields.data_len = data_transmit_bytes_in_frame + 1;
        for (int j = 0; j < data_transmit_bytes_in_frame; j++) {
          can_message_ptr.data.bytes[j + 1] = *(temp_data_ptr++);
        }
        can__tx(can, &can_message_ptr, timeout);

        consecutive_frame_index = (consecutive_frame_index + 1) % 16;
        // calculate_speration_time_in_us(message_param->fc_info.sep_time, &speration_time_in_us);
        delay__us(speration_time_in_us);
        tx_data_size_left -= 7;
        //printf("TX data left = %d\n", tx_data_size_left);
	if (tx_data_size_left <= 0) {
          result = true;
          break;
        }

        /*} else {
          for (int i = 0; i < blocks; i++) {
            can_message_ptr.data.bytes[0] = ((uint8_t)0x0F & consecutive_frame_index);
            can_message_ptr.data.bytes[0] |= ((uint8_t)0xF0 & (consecutive_frame << 4)); // byte 0, upper nibble

            for (int j = 0; j < 7; j++)
              can_message_ptr.data.bytes[j + 1] = *(temp_data_ptr++);

            can__tx(can, &can_message_ptr, timeout);
            result = true;
            consecutive_frame_index = (consecutive_frame_index + 1) % 16;
            calculate_speration_time_in_ms(message_param->fc_info.sep_time, &speration_time_in_ms);
            delay__ms(speration_time_in_ms);
          }*/
        // }
        // } // end of else if(message_param->fc_info.fc_flag == CF_continue_to_send)
        fw_check_frame++;
      } while (!is_fc_required || message_param->fc_info.fc_flag == CONTINUE);
    } // end of if(can__tx(can, &can_message_ptr, timeout))
  }   // end of else if (MAX_PDU_SIZE < data_size_in_bytes)

  return result;
}

void process_single_frame(isotp_params__s *message_param, can__msg_t msg_recieved) {
  message_param->recieve_buffer_offset = 0;
  uint8_t bytes_recieved = 1;
  // message_param->recieve_buffer = (uint8_t *)rcv_data_ptr;
  uint8_t size_of_recieved_msg = msg_recieved.data.bytes[0] & 0x0F;
  message_param->data_size = size_of_recieved_msg;
  message_param->fc_info.fc_flag = CONTINUE;
  message_param->frame_type = SINGLE_FRAME;
  // message_param->recieve_buffer_size = rcv_data_size;

  if (size_of_recieved_msg <= message_param->recieve_buffer_size) {
    while (bytes_recieved <= size_of_recieved_msg) {
      message_param->recieve_buffer[message_param->recieve_buffer_offset++] =
          (uint8_t)msg_recieved.data.bytes[bytes_recieved++];
    }
    message_param->fc_info.fc_flag = COMPLETE;
  }
}

void process_first_frame(isotp_params__s *message_param, can__msg_t msg_recieved) {
  message_param->data_size = ((msg_recieved.data.bytes[0] & 0x0F) | msg_recieved.data.bytes[1] << 4);
  printf("Data Size = %d\n", message_param->data_size);
  // message_param->recieve_buffer_size = rcv_data_size;
  // message_param->recieve_buffer = (uint8_t *)rcv_data_ptr;
  message_param->frame_id = 0;
  message_param->frame_type = FIRST_FRAME;
  message_param->recieve_buffer_offset = 0;
  message_param->fc_info.fc_flag = CONTINUE;

  for (uint8_t counter = 0; counter < 6; counter++) {
    message_param->recieve_buffer[message_param->recieve_buffer_offset++] = msg_recieved.data.bytes[counter + 2];
  }
  message_param->fc_info.fc_flag = CONTINUE;
  message_param->fc_info.block_size = 0;
  message_param->fc_info.sep_time = 0xF1;
//   isotp_tx(message_param, can1, &(message_param->fc_info), 3, true);
}

void process_consequtive_frame(isotp_params__s *message_param, can__msg_t msg_recieved) {
  message_param->frame_type = CONSECUTIVE_FRAME;
  message_param->frame_id = (msg_recieved.data.bytes[0] & 0x0F);
  message_param->fc_info.fc_flag = CONTINUE;
  uint32_t data_receive_size = ((message_param->data_size - message_param->recieve_buffer_offset) > 7)
                                   ? 7
                                   : (message_param->data_size - message_param->recieve_buffer_offset);

  for (uint8_t counter = 0; counter < data_receive_size; counter++) {
    message_param->recieve_buffer[message_param->recieve_buffer_offset++] = msg_recieved.data.bytes[counter + 1];
  }
  if ((int)message_param->data_size - (int)message_param->recieve_buffer_offset <= 0) {
    printf("Complete\n");
    message_param->fc_info.fc_flag = COMPLETE;
  }
}

void process_flowControl_frame(isotp_params__s *message_param, can__msg_t msg_recieved) {
  message_param->fc_info.fc_flag = msg_recieved.data.bytes[0] & 0x0F;
  message_param->fc_info.block_size = msg_recieved.data.bytes[1];
  message_param->fc_info.sep_time = msg_recieved.data.bytes[2];
}
// can__rx(can, &msg_recieved, timeout_ms)
void isotp_rx(isotp_params__s *message_param, can__msg_t msg_recieved) {

  uint8_t type_of_msg;

  // if () {
  type_of_msg = (msg_recieved.data.bytes[0] & 0xF0) >> 4;

  switch (type_of_msg) {
  case 0:
    printf("Single Frame\n");
    process_single_frame(message_param, msg_recieved);
    break;
  case 1:
    printf("First Frame\n");
    process_first_frame(message_param, msg_recieved);
    break;
  case 2:
    printf("Consecutive Frame\n");
    process_consequtive_frame(message_param, msg_recieved);
    break;
  case 3:
    printf("FlowControl Frame\n");
    process_flowControl_frame(message_param, msg_recieved);
    break;
  default:
    break;
  }
  // }
}
