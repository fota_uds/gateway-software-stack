
#include <assert.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>

#include "can_handler.h"
#include "isotp.h"
#include <linux/can.h>
#include <linux/can/raw.h>
#include <time.h>
#include "uds.h"

static bool is_reset_sucessfull = false;
static bool is_flash_erase_successfull = false;
static bool is_flash_successfull = false;
static bool is_status_valid = false;
static bool is_flashing = false;

char speed_car[16] = {};
static bool is_speed_received = false;

void *bb_can_receive(void *ptr) {
  assert(can__init());
  can__msg_t can_receive_msg = {};
  isotp_params__s rx_params = {};
  isotp__init(&rx_params, &speed_car, 8);
  while (1) {
    if(can__rx(&can_receive_msg)){
      //printf("Can message received  =%d\n", can_receive_msg.msg_id);
      if(can_receive_msg.msg_id == 400+40 && is_status_valid){
        is_reset_sucessfull = true;
      }
      else if(can_receive_msg.msg_id == 405+40 && is_status_valid){
        is_flash_erase_successfull = true;
      }
      else if(can_receive_msg.msg_id == 406+40 && is_status_valid){
	is_flashing = true;
      }
      else if(can_receive_msg.msg_id == 407+40 && is_status_valid){
        is_flash_successfull = true;
      }
      else if(can_receive_msg.msg_id == 401+40){
	      puts("data received");
	      isotp_rx(&rx_params, can_receive_msg);
	      is_speed_received = true;
      }
      //sleep(1);
      //else if(can_receive_msg.msg_id == 0x7f)
        //TODO: Handle negative response frame
    }
  }
  //     if(can_receive_msg.msg_id==400){
  //       isotp_rx(&rx_params, can_receive_msg);
  //       if (rx_params.fc_info.fc_flag == COMPLETE) {
  //         for (int i = 0; i < rx_params.data_size; i++){
  //           printf("Data = %d\n", rx_data[i]);
  //           rcv[i] = rx_data[i];
  //         }
  //         break;
  //       }
  //       else if(can_receive_msg.msg_id==401){
  //       }
  //     }
  //   }
  // }
}

void bb_init_transmit(uint8_t ecu_id) {
  assert(can__init());
  uint8_t tx_data[2];
  tx_data[0] = 1;
  tx_data[1] = ecu_id;
  isotp_params__s tx_param = {};
  tx_param.CAN_ID = 405;
  isotp_tx(&tx_param, can1, tx_data, 2, false);
}

int bb_firmware_transmit(char *filename) {
  assert(can__init());

  FILE *file = NULL;
  uint8_t tx_data[1024]; // array of bytes, not pointers-to-bytes
  size_t bytesRead = 0;

  file = fopen(filename, "rb");

  if (file != NULL) {
    uint16_t frame_number=0;
    while ((bytesRead = fread(tx_data, 1, 1024, file)) > 0) {
      //printf("Bytes read = %d\n", bytesRead);
      bool no_ack = false;
      do{
	no_ack=false;
        isotp_params__s tx_param = {};
        tx_param.CAN_ID = 406;
        if(frame_number == 0){
      	  uint32_t *s_ptr = (uint32_t *)(tx_data + 4);
       	  printf("App Start address = %x\n", *s_ptr);
        }
        is_status_valid=true;
        isotp_tx(&tx_param, can1, tx_data, bytesRead, false);
        memset(tx_data, 0, sizeof(tx_data));
        printf("Transmitted Frame %d\n", frame_number++);
  	time_t current_time = time(NULL);
  	while(!is_flashing && (time(NULL)-current_time) < 30);
        if(!is_flashing){
      	  fprintf(stderr, ".Retransmiting");
	  no_ack = true;
	  frame_number--;
        }
        is_flashing=false;
        is_status_valid = false;
        //sleep(1);
      }while(no_ack);
    }
  }
}

int bb_finish_transmit() {
  assert(can__init());
  uint8_t tx_data[2];
  uint8_t temp = 0;
  for (int i = 0; i < 2; i++) {
    tx_data[i] = temp;
    temp++;
  }
  isotp_params__s tx_param = {};
  tx_param.CAN_ID = 407;
  isotp_tx(&tx_param, can1, tx_data, 2, false);
}

void reset_ecu(uint8_t ecu_id) {
  assert(can__init());
  uint8_t tx_data[2];
  tx_data[0] = 0x01;
  tx_data[1] = ecu_id;
  isotp_params__s tx_param = {};
  tx_param.CAN_ID = 400;
  isotp_tx(&tx_param, can1, tx_data, 2, false);
}

int8_t reset_service(uint8_t ecu_id, char *filename) {
  is_status_valid=true;
  reset_ecu(ecu_id);
  //return 0;
  uint32_t count = 0xffffff;
  printf("Reset Service Called = %d\n", is_reset_sucessfull);
  time_t current_time = time(NULL);
  while(!is_reset_sucessfull && (time(NULL)-current_time) < 30);
  if(!is_reset_sucessfull)
    return -1;
  else
    is_reset_sucessfull = false;
  printf("Reset is sucessfull\n");
  bb_init_transmit(ecu_id);
  current_time = time(NULL);
  count = 0xffffff;
  printf("Reset Service Called = %d\n", is_reset_sucessfull);
  while(!is_flash_erase_successfull && time(NULL)-current_time <2);
  if(!is_flash_erase_successfull){
    printf("erase not sucess\n");
    return -2;
  }
  else
    is_flash_erase_successfull = false;
  sleep(1);
  printf("flash erase success\n");
  bb_firmware_transmit(filename);
  bb_finish_transmit();
  count = 0xffffff;
  printf("Finish Transmit = %d\n", is_reset_sucessfull);
  current_time = time(NULL);
  while(!is_flash_successfull && (time(NULL)-current_time) < 40);
  if(!is_flash_successfull)
    return -3;
  else
    is_flash_successfull = false;
  is_status_valid=false;
  return 0;
}


//TODO: fix this function
char* request_data_by_id(uint8_t ecu_id, uint8_t data_id) {
  assert(can__init());
  uint8_t tx_data[3];
  tx_data[0] = 0x02;
  tx_data[1] = ecu_id;
  tx_data[2] = data_id;
  isotp_params__s tx_param = {};
  tx_param.CAN_ID = 401;
  isotp_tx(&tx_param, can1, tx_data, 3, false);
  //uint8_t rcv_data[8];
  time_t current_time = time(NULL);
  while(!is_speed_received && (time(NULL)-current_time) < 2);
  if(is_speed_received)
  	is_speed_received = false;
  else{
	strcpy(speed_car, "NaN");
        printf("data not received\n");
  }
  char *str_data = (char *) malloc(9*sizeof(char));
  int i=0;
  for(i=0;i<8;i++){
  	str_data[i] = speed_car[i]; 
  }
  str_data[i] = '\0';
  return str_data;
  // receive();
}

