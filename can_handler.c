#include "can_handler.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <assert.h>

#include <net/if.h>
#include <sys/ioctl.h>
#include <sys/socket.h>

#include <linux/can.h>
#include <linux/can/raw.h>


static bool is_can_initialized = false;

int s;

bool can__init(){
	struct sockaddr_can addr;
	struct ifreq ifr;
	if(is_can_initialized){
		return true;
	}

	printf("CAN init\r\n");

	if ((s = socket(PF_CAN, SOCK_RAW, CAN_RAW)) < 0) {
		perror("Socket");
		return false;
	}

	strcpy(ifr.ifr_name, "can1" );
	ioctl(s, SIOCGIFINDEX, &ifr);

	memset(&addr, 0, sizeof(addr));
	addr.can_family = AF_CAN;
	addr.can_ifindex = ifr.ifr_ifindex;

	if (bind(s, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
		perror("Bind");
		return false;
	}
	is_can_initialized = true;
	return true;
}

bool can__tx(can__num_e can, can__msg_t *msg_ptr, uint32_t timeout_ms)
{
	assert(is_can_initialized);
	// int s;
	struct can_frame frame;
	frame.can_id = msg_ptr->msg_id;
	frame.can_dlc = msg_ptr->frame_fields.data_len;
	for(int i=0;i<8;i++){
		frame.data[i] = msg_ptr->data.bytes[i];
	}
	// sprintf(frame.data, "Hello");
        // printf("Hello\n");
	if (write(s, &frame, sizeof(struct can_frame)) != sizeof(struct can_frame)) {
		perror("Write");
		return false;
	}

	// if (close(s) < 0) {
	// 	perror("Close");
	// 	return false;
	// }

	return true;
}


// can__msg_t can__rx(void)
bool can__rx(can__msg_t *can_receive_msg)
{
	// assert(is_can_initialized);
	int i; 
	int nbytes;
	struct sockaddr_can addr;
	struct ifreq ifr;
	struct can_frame frame;

	//printf("CAN Message received\r\n");
	nbytes = read(s, &frame, sizeof(struct can_frame));
	if (nbytes < 0) {
		perror("Read");
		return false;
	}
	can_receive_msg->msg_id = frame.can_id;
	can_receive_msg->frame_fields.data_len = frame.can_dlc;
#if(0)
	if(frame.can_id >= 400 && frame.can_id <450)
	    printf("%d - [%d]\n",frame.can_id, frame.can_dlc);
#endif
	for (i = 0; i < frame.can_dlc; i++){
		can_receive_msg->data.bytes[i] = frame.data[i];
		//printf("%02X ",frame.data[i]);
	}
	
	//printf("\r\n");
	return true;
}

