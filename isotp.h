// isotp.h
#pragma once
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

typedef union {
  uint64_t qword;     ///< All 64-bits of data of a CAN message
  uint32_t dwords[2]; ///< dwords[0] = Byte 0-3,   dwords[1] = Byte 4-7
  uint16_t words[4];  ///<  words[0] = Byte 0-1 ... words[3] = Byte 6-7
  uint8_t bytes[8];   ///< 8 bytes of a CAN message
} can__data_t;

/**
 * Type definition of a CAN message with bit-fields (assuming little-endian machine)
 * DO NOT CHANGE THIS STRUCTURE - it maps to the hardware
 */
typedef struct {
  union {
    uint32_t frame; ///< 32-bit CAN frame aligned with frame_fields (bit members)
    struct {
      uint32_t : 16;         ///< Unused space
      uint32_t data_len : 4; ///< Data length
      uint32_t : 10;         ///< Unused space
      uint32_t is_rtr : 1;   ///< Message is an RTR type
      uint32_t is_29bit : 1; ///< If message ID is 29-bit type
    } frame_fields;
  };

  uint32_t msg_id;  ///< CAN Message ID (11-bit or 29-bit)
  can__data_t data; ///< CAN data
} __attribute__((__packed__)) can__msg_t;


typedef enum frame_type__s {
  SINGLE_FRAME,
  FIRST_FRAME,
  CONSECUTIVE_FRAME,
  FLOW_CONTROL_FRAME,
} frame_type__s;

typedef enum frame_status__s {
  CONTINUE,
  WAIT,
  ABORT,
  COMPLETE,
} frame_status__s;

typedef struct flow_control_s {
  frame_status__s fc_flag;
  uint8_t block_size;
  uint8_t sep_time;
} flow_control_s;

typedef struct isotp_params__s {
  uint16_t CAN_ID;
  uint8_t recieve_buffer_size;
  uint8_t *recieve_buffer;
  uint16_t data_size;
  flow_control_s fc_info;
  uint32_t recieve_buffer_offset;
  uint8_t frame_id;
  frame_type__s frame_type;
} isotp_params__s;

typedef enum {
  can1,    ///< CAN #1
  can2,    ///< CAN #2
  can_max, ///< Do not use or change
} can__num_e;

void isotp__init(isotp_params__s *message_param, void *rcv_data_ptr, size_t rcv_data_size_in_bytes);

bool isotp_tx(isotp_params__s *message_param, can__num_e can, void *data_ptr, size_t data_size, bool is_control_frame);
// check size of data
void isotp_rx(isotp_params__s *message_param, can__msg_t msg_recieved);

